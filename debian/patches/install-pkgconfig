From: Dennis Filder <d.filder@web.de>
Date: Fri, 7 Feb 2025 10:11:27 +0100
Subject: Configure and install pkg-config and cmake files for bzrtp

Without this the build scripts try to install cmake files under
/usr/share which causes multi-arch problems.
---
 CMakeLists.txt | 11 ++++++++++-
 1 file changed, 10 insertions(+), 1 deletion(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index da43ab8..13b7719 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -125,8 +125,17 @@ if(ENABLE_PACKAGE_SOURCE)
 	add_subdirectory(build)
 endif()
 
+set(prefix ${CMAKE_INSTALL_PREFIX})
+set(exec_prefix ${prefix}/bin)
+set(libdir ${prefix}/lib)
+set(includedir ${prefix}/include)
+set(PACKAGE_VERSION "${BZRTP_VERSION}")
+
+configure_file(${CMAKE_CURRENT_SOURCE_DIR}/libbzrtp.pc.in ${CMAKE_CURRENT_BINARY_DIR}/libbzrtp.pc)
+install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libbzrtp.pc DESTINATION "${CMAKE_INSTALL_LIBDIR}/pkgconfig")
+
 include(CMakePackageConfigHelpers)
-set(CMAKE_MODULES_INSTALL_DIR "${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}/cmake")
+set(CMAKE_MODULES_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")
 configure_package_config_file("cmake/${PROJECT_NAME}Config.cmake.in" "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
 	INSTALL_DESTINATION "${CMAKE_MODULES_INSTALL_DIR}"
 	NO_SET_AND_CHECK_MACRO
